/*
 * command.c
 *
 *  Created on: 2018. aug. 20.
 *      Author: zoli
 */

#include "command.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "usb_fifo.h"
#include <stdarg.h>
#include "usbd_cdc_if.h"


CMD_CommandTypeDef cmd_array[CMD_MAX_NUMCOMMANDS];	// array of the command definitions

// Register a command for processing
void CMD_RegisterCommand(CMD_CommandTypeDef command)
{
	cmd_array[command.CommandCode] = command;
}

// Register a device for command responses
void CMD_RegisterDevice(CMD_ResponseTypeDef device)
{

}

// Call a command based on a command string
// The command is a hexadecimal command code plus arguments separated by commas
void CMD_Call(char* cmdstr, uint8_t source)
{
	char tmpstr[strlen(cmdstr)+1]; // placeholder of the copied string
	int i;
	uint8_t cmdcode;	// command code
	char* tmpptr;		// dummy pointer - used by the strto functions
	char* argstr;		// pointer to the current argument
	CMD_CommandTypeDef currcmd;	// pointer to the current command deffinition structure
//	char responsebuff[80];	// String buffer for USB response (test code until the other output types written)
//	uint16_t responselen;	// Length of the USB response (test code until the other output types written)

	// Copy the original string. The string manipulation bellow is destructive, so use a copy strng instead
	strcpy(tmpstr,cmdstr);

	// parse command
	char* commandstr = strtok(tmpstr,",");
	cmdcode = strtol(commandstr,&tmpptr,16); // using hexadecimal commands
	currcmd = cmd_array[cmdcode];
//	double retdouble;

	// parse parameters
	void* args[currcmd.NumParams];

	for(i=0;i<currcmd.NumParams;i++)
	{
		// get the next token from the string
		argstr=strtok(NULL, ",");
		// convert based on the expected argument type
		switch(currcmd.ParamArr[i])
		{
			case CMD_TYPE_UINT8:
				args[i] = alloca(1);
				*((uint8_t*)args[i]) = (uint8_t)strtol(argstr,&tmpptr,10);
				break;
			case CMD_TYPE_UINT16:
				args[i] = alloca(2);
				*((uint16_t*)args[i]) = (uint16_t)strtol(argstr,&tmpptr,10);
				break;
			case CMD_TYPE_UINT32:
				args[i] = alloca(4);
				*((uint32_t*)args[i]) = (uint32_t)strtol(argstr,&tmpptr,10);
				break;
			case CMD_TYPE_DOUBLE:
				args[i] = alloca(8);
				*((double*)args[i]) = strtod(argstr,&tmpptr);
				break;
		}
	}
	currcmd.CommandProcessor(source, args,currcmd.NumParams);
	/*
	// call the function
	switch(currcmd.ResponseType)
	{
		case CMD_TYPE_NONE:
			currcmd.CommandProcessor(args,currcmd.NumParams);
			break;
		case CMD_TYPE_DOUBLE:
			retdouble = currcmd.CommandProcessor(args,currcmd.NumParams);
			char* replacestring = "%c,%" + (currcmd.ResponseDigits != 0 ? currcmd.ResponseDigits : '') + "." + currcmd.ResponseFrac + "f\r";
			responselen = sprintf(responsebuff, "%c,%.3f\r",currcmd.ResponseCode,retdouble);
			break;
	}
	if(currcmd.ResponseType != CMD_TYPE_NONE)
	{
		USB_CDC_TX_write((uint8_t*)responsebuff, responselen);
	}
	*/
}

void CMD_Response_Empty(char* template, char command)
{
	char buff[80];
	uint16_t len;
	len = sprintf(buff, template, command);
	USB_CDC_RX_WaitforReady();
	if(USB_CDC_Enabled)
	{
		USB_CDC_TX_write((uint8_t*)buff, len);
	}
}

void CMD_Response_Raw(uint8_t* buff, uint16_t len)
{
	USB_CDC_RX_WaitforReady();
	if(USB_CDC_Enabled)
	{
		USB_CDC_TX_write(buff, len);
	}
}

void CMD_Response_LF()
{
	char lf = '\r';
	USB_CDC_RX_WaitforReady();
	if(USB_CDC_Enabled)
	{
		USB_CDC_TX_write((uint8_t*)&lf, 1);
	}
}

void CMD_Response(char* template, ...)
{
	char buff[80];
	uint16_t len;
	va_list args;
	va_start(args, template);
	len = vsprintf(buff, template, args);
	va_end(args);
	USB_CDC_RX_WaitforReady();
	if(USB_CDC_Enabled)
	{
		USB_CDC_TX_write((uint8_t*)buff, len);
	}
}

