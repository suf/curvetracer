/*
 * sine.h
 *
 *  Created on: 2018. aug. 5.
 *      Author: zoli
 */

#include <stdint.h>

#ifndef WFG_H_
#define WFG_H_

#define WFG_PRESET_20Hz  0
#define WFG_PRESET_30Hz  1
#define WFG_PRESET_40Hz  2
#define WFG_PRESET_50Hz  3
#define WFG_PRESET_60Hz  4
#define WFG_PRESET_70Hz  5
#define WFG_PRESET_80Hz  6
#define WFG_PRESET_90Hz  7
#define WFG_PRESET_100Hz  8
#define WFG_PRESET_110Hz  9
#define WFG_PRESET_120Hz  10
#define WFG_PRESET_130Hz  11
#define WFG_PRESET_140Hz  12
#define WFG_PRESET_150Hz  13
#define WFG_PRESET_160Hz  14
#define WFG_PRESET_170Hz  15
#define WFG_PRESET_180Hz  16
#define WFG_PRESET_190Hz  17
#define WFG_PRESET_200Hz  18
#define WFG_PRESET_300Hz  19
#define WFG_PRESET_400Hz  20
#define WFG_PRESET_500Hz  21
#define WFG_PRESET_600Hz  22
#define WFG_PRESET_700Hz  23
#define WFG_PRESET_800Hz  24
#define WFG_PRESET_900Hz  25
#define WFG_PRESET_1000Hz  26
#define WFG_PRESET_1100Hz  27
#define WFG_PRESET_1200Hz  28
#define WFG_PRESET_1300Hz  29
#define WFG_PRESET_1400Hz  30
#define WFG_PRESET_1500Hz  31
#define WFG_PRESET_1600Hz  32
#define WFG_PRESET_1700Hz  33
#define WFG_PRESET_1800Hz  34
#define WFG_PRESET_1900Hz  35
#define WFG_PRESET_2000Hz  36
#define WFG_PRESET_3000Hz  37
#define WFG_PRESET_4000Hz  38
#define WFG_PRESET_5000Hz  39

#define WFG_MAX_SAMPLES 500
#define WFG_BASE_FREQ 84000000

#define WFG_CAL_ZERO	0
#define WFG_CAL_PLUS	1
#define WFG_CAL_MINUS	-1

#define WFG_MODE_SINE 0
#define WFG_MODE_CAL 1

void WFG_SetNumSamples(uint16_t samples);
void WFG_Stop();
void WFG_Start();
void WFG_SetTimerPeriod(uint16_t period);
void WFG_SelectFreq(uint8_t index);
void WFG_SetFreq(double freq);
void WFG_GetFreq();
void WFG_Mode(uint8_t mode, uint8_t level);
void WFG_SetCalSwing(uint16_t swing);
void WFG_SetCalPoint(uint16_t point);
void WFG_GetCalData();

void WFG_Init();


#endif /* WFG_H_ */
