/*
 * SCREEN.h
 *
 *  Created on: 2018. nov. 27.
 *      Author: zoli
 */

#ifndef SCREEN_H_
#define SCREEN_H_

#include "lvgl.h"

#define SCREEN_SCOPE_PERIOD_MS 200

void SCREEN_Init();
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
void SCREEN_CreateLog(lv_obj_t * parent);
void SCREEN_Log(const char * logtext);

void SCREEN_CreateScope();
void SCREEN_Scope_Setup();
void SCREEN_Scope_AddPoint(uint16_t index, uint16_t voltage, uint16_t current);
void SCREEN_Scope_Display();

#endif /* SCREEN_H_ */
