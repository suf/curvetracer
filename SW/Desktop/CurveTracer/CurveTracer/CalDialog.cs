﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurveTracer
{
    public partial class CalDialog : Form
    {
        SerialCom _com;
        WFG_MODE WfgMode;
        bool IsDataFromDevice; // the values comming from the device, don't write back to the device
        int level;
        public CalDialog(SerialCom com)
        {
            InitializeComponent();
            this._com = com;
            level = int.Parse((string)this.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Tag);
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCalMode_Click(object sender, EventArgs e)
        {
            WfgMode = WFG_MODE.CAL;
            SetMode();
        }

        private void btnNormalMode_Click(object sender, EventArgs e)
        {
            WfgMode = WFG_MODE.SINE;
            SetMode();

        }

        // Used by all RadioButtons on the form - virtually named GroupLevel
        private void rbtGroupLevel_CheckedChanged(object sender, EventArgs e)
        {
            int tmplevel;
            tmplevel = int.Parse((string)this.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Tag);
            if (level != tmplevel)
            {
                SetMode();
            }
            level = tmplevel;
        }

        private void udNullPoint_ValueChanged(object sender, EventArgs e)
        {
            if (!IsDataFromDevice)
            {
                // MessageBox.Show("Value Changed");
                SetCalPoint();
            }
        }

        private void udSwing_ValueChanged(object sender, EventArgs e)
        {
            if (!IsDataFromDevice)
            {
                SetCalSwing();
            }
        }
        private void SetMode()
        {
            if (_com.IsOpen)
            {
                 _com.SendCommand(CommandCode.WFG_MODE, new string[] { ((int)WfgMode).ToString(), (string)this.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Tag });
            }
            else
            {
                MessageBox.Show(((int)CommandCode.WFG_MODE).ToString("x") + "," + ((int)WfgMode).ToString() + "," + (string)this.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Tag);
            }
        }
        private void SetCalSwing()
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.WFG_SETCALSWING, udSwing.Value.ToString());
            }
            else
            {
                MessageBox.Show(((int)CommandCode.WFG_SETCALSWING).ToString("x") + "," + udSwing.Value.ToString());
            }
        }
        private void SetCalPoint()
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.WFG_SETCALPOINT, udNullPoint.Value.ToString());
            }
            else
            {
                MessageBox.Show(((int)CommandCode.WFG_SETCALPOINT).ToString("x") + "," + udNullPoint.Value.ToString());
            }
        }
        private void GetCalData()
        {
            if (_com.IsOpen)
            {
                _com.SendCommand(CommandCode.WFG_GETCALDATA);
            }
            // Dry testing - no device connected
            // Update_CalData("1234,2345");
        }
        public void Update_CalData(string[] dataarr)
        {
            int NullPoint;
            int Swing;
            IsDataFromDevice = true;
            if(dataarr.Length == 2)
            {
                if(int.TryParse(dataarr[0], out NullPoint))
                {
                    if (this.udNullPoint.Value != NullPoint && NullPoint >= this.udNullPoint.Minimum && NullPoint <= this.udNullPoint.Maximum)
                    {
                        // this.udNullPoint.Value = NullPoint;
                        SetNullPoint(NullPoint);
                    }
                }
                if (int.TryParse(dataarr[1], out Swing))
                {
                    if (this.udSwing.Value != Swing && Swing >= this.udSwing.Minimum && Swing <= this.udSwing.Maximum)
                    {
                        // this.udSwing.Value = Swing;
                        SetSwing(Swing);
                    }
                }
            }
            IsDataFromDevice = false;
        }
        private void SetNullPoint(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<int>(SetNullPoint), new object[] { value });
                return;
            }
            this.udNullPoint.Value = value;
        }

        private void SetSwing(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<int>(SetSwing), new object[] { value });
                return;
            }
            this.udSwing.Value = value;
        }

        private void CalDialog_Shown(object sender, EventArgs e)
        {
            GetCalData();
        }

        private void CalDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // switch back to sine wave generation, then close
            WfgMode = WFG_MODE.SINE;
            SetMode();
        }
    }
}
